#!/usr/bin/env node
const publishPackage = require('..');
const zipdir = require('zip-dir');

const filePath = process.argv[2];

zipdir(filePath, (err, buffer) => {
  if (err) console.error(err);
  const fileName = process.argv[3];
  const teams = process.argv.some(arg => arg === '--teams' || arg === '-s');

  publishPackage(buffer, fileName, { teams })
    .then(console.log)
    .catch(console.error);
});
