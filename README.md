# PUBLISH PACKAGE #
> Biblioteca para publicar arquivos no owncloud

## Instalação

##### Como dependência do projeto

```
npm install bitbucket:onedigital/publish-package
# ou
yarn add bitbucket:onedigital/publish-package
```

##### Usando globalmente
```
$ publish-package caminho/para/sua/pasta nome-do-arquivo.zip --teams
```

## Usando

Dentro da raiz do seu projeto crie o arquivo `.env` com os seguintes valores:
```env
USER_OWNCLOUD='usuario'
PASSWORD_OWNCLOUD='senha'
TEAMS_WEBHOOK='URL'
```

Dentro do seu arquivo `.js`:
```js
const publishPackage = require('publish-package');

const buffer = fs.createReadStream('caminho/para/seu/arquivo');

publishPackage(buffer, 'MEU-PACOTE.zip');
```


### publishPackage(path, fileName ,[options])

### path
Type: `string | buffer | url`

#### filename

Type: `string`

#### options

Type: `Object`


### Default options

Se nenhum valor for passado alguns valores serão setados automaticamente

Option            | Value
----------------- | --------------------------------------------------------
`teams` | `false` (quando for verdadeiro ele irá mandar notificação no teams)
`password`          | `string aleatória`
`pathOwnCloud`      | `TESTE-TI` (pasta onde será salvo o arquivo no owncloud)
`deleteWhenFinish`  | `false` (caso deseje deletar o pacote, no ambiente de teste esssa opção tem o valor de `true`)
