const { readFile } = require('fs');
const fetch = require('node-fetch');
const generator = require('generate-password');

const getDataFromFile = fileName => new Promise(((resolve, reject) => {
  readFile(fileName, (err, data) => {
    err ? reject(err) : resolve(data);
  });
}));

const getFileFromUrl = async url => (await fetch(url)).buffer();

/**
 *
 * @param {String} str
 */
const isURL = str => /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/.test(str);

/**
 * Gera uma autenticação básica
 * @param {String} str
 */
const generateBasicAuth = (user, password) => `Basic ${Buffer.from(`${user}:${password}`).toString('base64')}`;

/**
 * Gera uma senha aleatória
 */
const generatePassword = () => generator.generate({ length: 8, numbers: true });

/**
 * Gera um buffer se o parâmetro for um caminho para o arquivo
 * @param {*} path
 */
const generateBuffer = async (path) => {
  if (isURL(path)) return getFileFromUrl(path);
  return (typeof path === 'string' ? getDataFromFile(path) : path);
};


/**
 *
 * @param {String} str
 */
const isNotDefaultEncryptionModule = str => str === 'No default encryption module defined';


/**
 * Gera a frase para um novo pacote compartilhado
 * @param {Object} obj
 */
const generateNewPackageText = ({ url, password, fileName }) => `
      &#x1F44A; **Arquivo ${fileName.replace(/_/gi, '\u005C\u005F')} publicado:**  
      Link: [${url}](${url})  
      Senha: ${password}`
  ;

/**
 * Gera a frase para pacotes que já foram compartilhados
 * @param {String} fileName
 */
const generateAlreadySharedPackageText = fileName => ` &#x1F364; **Arquivo ${fileName.replace(/_/gi, '\u005C\u005F')} foi sobrescrito** `;

const checkStatus = (meta) => {
  if (meta.status !== 'ok') throw meta;
};

module.exports = {
  generateBuffer,
  generateBasicAuth,
  generatePassword,
  isNotDefaultEncryptionModule,
  generateAlreadySharedPackageText,
  generateNewPackageText,
  checkStatus,
  isURL,
  getDataFromFile,
};
