const fetch = require('node-fetch');
const { Agent } = require('https');
require('dotenv').config();

const {
  USER_OWNCLOUD, PASSWORD_OWNCLOUD, TEAMS_WEBHOOK, NODE_ENV,
} = process.env;
const {
  generateBasicAuth,
  generateBuffer,
  generatePassword,
  isNotDefaultEncryptionModule,
  generateNewPackageText,
  generateAlreadySharedPackageText,
  checkStatus,
} = require('./utils');

const SERVER_URI = 'https://ftpcloud.one.com.br/owncloud';
const UPLOAD_API_PATH = 'remote.php/webdav';
const SHARE_API_PATH = 'ocs/v1.php/apps/files_sharing/api/v1/shares';


const agent = Agent({ rejectUnauthorized: false });

const basicAuthOwnCloud = generateBasicAuth(USER_OWNCLOUD, PASSWORD_OWNCLOUD);
/**
 * Sobe um arquivo para o owncloud
 * @param { Buffer } buffer
 * @param { String } fileDestination
 */
const uploadFile = (buffer, fileDestination) => fetch(`${SERVER_URI}/${UPLOAD_API_PATH}/${fileDestination}`, {
  body: buffer,
  agent,
  headers: {
    Authorization: basicAuthOwnCloud,
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  method: 'PUT',
});

const getSharesFromASpecificFile = async (fileDestination) => {
  const res = await fetch(`${SERVER_URI}/${SHARE_API_PATH}?path=/${fileDestination}&reshares=true&format=json`, {
    agent,
    headers: {
      Authorization: basicAuthOwnCloud,
    },
  });

  return res.json();
};

const isFileAlreadyShared = previousShare => previousShare.ocs.data.length > 0;

/**
 * Compartilha um arquivo existente
 * @param {String} fileDestination
 * @param {String} password
 */
const shareFile = async (fileDestination, password) => {
  const uri = `${SERVER_URI}/${SHARE_API_PATH}?format=json`;
  const options = {
    body: `path=${fileDestination}&shareType=3&permissions=1&password=${password}`,
    agent,
    headers: {
      Authorization: basicAuthOwnCloud,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
  };

  let json = await (await fetch(uri, options)).json();

  if (isNotDefaultEncryptionModule(json.ocs.meta.message)) {
    json = await shareFile(fileDestination, password);
  }

  checkStatus(json.ocs.meta);
  return json;
};

const sendTeamsNotification = text => fetch(TEAMS_WEBHOOK, {
  body: JSON.stringify({ text }),
  headers: {
    'Content-Type': 'application/json',
  },
  method: 'POST',
});

const deletePackage = async fileDestination =>
  fetch(`${SERVER_URI}/${UPLOAD_API_PATH}/${fileDestination}`, {
    agent,
    headers: {
      Authorization: basicAuthOwnCloud,
    },
    method: 'DELETE',
  });


const getTeamsText = shareInfo => (
  shareInfo.fileAlreadyShared
    ? generateAlreadySharedPackageText(shareInfo.fileName)
    : generateNewPackageText(shareInfo)
);

/**
 * Publica e compartilha o arquivo no owncloud retorna o link e a senha do arquivo
 * @param { String | Buffer | URL} path
 * @param { string} fileName
 * @param { Object } options
 */
const publishPackage = async (path, fileName, {
  teams = true,
  password = generatePassword(),
  pathOwnCloud = 'TESTE-TI',
  fileDestination = `${pathOwnCloud}/${fileName}`,
  deleteWhenFinish = NODE_ENV === 'test',
} = {}) => {
  try {
    if (fileName === '' || !fileName) throw new Error('Filename must be passed');
    const previousShare = await getSharesFromASpecificFile(fileDestination);
    const fileAlreadyShared = isFileAlreadyShared(previousShare);

    const shareInfo = { fileName, fileAlreadyShared };

    const buffer = await generateBuffer(path);

    await uploadFile(buffer, fileDestination);


    if (!fileAlreadyShared) {
      const { ocs: { data, meta } } = await shareFile(fileDestination, password);
      Object.assign(shareInfo, {
        ...meta, ...data, password,
      });
    } else {
      checkStatus(previousShare.ocs.meta);
      Object.assign(shareInfo, previousShare.ocs.data.pop());
    }

    if (teams) await sendTeamsNotification(getTeamsText(shareInfo));

    if (deleteWhenFinish) await deletePackage(fileDestination);

    return shareInfo;
  } catch (error) {
    return { error };
  }
};

module.exports = publishPackage;
