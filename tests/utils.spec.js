const {
  generateBasicAuth,
  generatePassword,
  generateAlreadySharedPackageText,
  generateNewPackageText,
  generateBuffer,
  isURL,
  checkStatus,
  isNotDefaultEncryptionModule,
  getDataFromFile,
} = require('./../src/utils');

describe('utils', () => {
  describe('generateBasicAuth', () => {
    it('should contain "Basic" in the basic auth', () => {
      expect(generateBasicAuth('user', 'pass')).toContain('Basic');
    });

    it('should return "Basic YTph" when user equal "a" and password equal "a"', () => {
      expect(generateBasicAuth('a', 'a')).toBe('Basic YTph');
    });
  });

  describe('generatePassword', () => {
    it('should generate a new password', () => {
      expect(typeof generatePassword()).toBe('string');
    });
    it('should have 8 characters', () => {
      expect(generatePassword().length).toBe(8);
    });
  });

  describe('generateAlreadySharedPackageText', () => {
    it('should return an string that constains the filename', () => {
      expect(generateAlreadySharedPackageText('filename.zip')).toContain('filename.zip');
    });
    it('should return an string that does not contain the password', () => {
      expect(generateAlreadySharedPackageText('filename.zip')).toMatch(/^((?!senha).)*$/gim);
    });
  });

  describe('generateNewPackageText', () => {
    const shareInfo = {
      url: 'www.google.com',
      password: '1a2s5a',
      fileName: 'filename.zip',
    };
    it('should return an string that constains the url, password and fileName', () => {
      const text = generateNewPackageText(shareInfo);
      expect(text).toContain(shareInfo.fileName);
      expect(text).toContain(shareInfo.password);
      expect(text).toContain(shareInfo.url);
    });
  });

  describe('generateBuffer', () => {
    it('should return a buffer when is passed a path', async () => {
      const buffer = await generateBuffer('./mocks/file.txt');

      expect(Buffer.isBuffer(buffer)).toBe(true);
    });

    it('should return a buffer when is passed a buffer', async () => {
      const buffer = Buffer.from(['aa']);

      const expected = await generateBuffer(buffer);

      expect(Buffer.isBuffer(expected)).toBe(true);
    });

    it('should return a buffer when is passed a url', async () => {
      const buffer = await generateBuffer('http://www.mocky.io/v2/5afc5475310000cb327c5dee');

      expect(Buffer.isBuffer(buffer)).toBe(true);
    });
  });

  describe('isUrl', () => {
    it('should return true if is a valid url', () => {
      expect(isURL('https://github.com/')).toBeTruthy();
      expect(isURL('http://banco-bradesco-hml.one.com.br/cartao-aeternum/facebook.png')).toBeTruthy();
    });

    it('should return true if is an invalid url', () => {
      expect(isURL('hthub.com/')).toBeFalsy();
      expect(isURL('htts://github.com/')).toBeFalsy();
      expect(isURL('https://githu')).toBeFalsy();
    });
  });

  describe('checkStatus', () => {
    it('should throw an Eror when meta.status is not equal "ok"', () => {
      const meta = { status: 'error' };
      expect(() => {
        checkStatus(meta);
      }).toThrow();
    });
    it('should not throw an Eror when meta.status is equal "ok"', () => {
      const meta = { status: 'ok' };
      expect(checkStatus(meta)).toBe(undefined);
    });
  });

  describe('isNotDefaultEncryptionModule', () => {
    it('should return true when is default encryption module', () => {
      expect(isNotDefaultEncryptionModule('No default encryption module defined')).toBe(true);
    });
    it('should return false when is not default encryption module', () => {
      expect(isNotDefaultEncryptionModule('teste')).toBe(false);
    });
  });

  describe('getDataFromFile', () => {
    it('should return the Buffer from a file', async () => {
      const data = await getDataFromFile('mocks/file.txt');

      expect(Buffer.isBuffer(data)).toBeTruthy();
    });

    it('should throw an error when file does not exist', (done) => {
      getDataFromFile('mocks/file.txtaaaaaaaaaaaaa')
        .catch((err) => {
          expect(err).toBeTruthy();
          done();
        });
    });
  });
});
