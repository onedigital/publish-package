const publishPackage = require('../src/publish-package');
const { isURL } = require('../src/utils');
const { createReadStream } = require('fs');


// publishPackage('http://files.one.com.br/bradescocartoes/emkt/18_102_j_5_regua_ativacao_cartoes_email_prime_v_03/pacote.zip', 'teste.zip')
//   .then(console.log);

jest.setTimeout(20000);

describe('publishPackage', () => {
  const fileName = 'text.txt';
  const mockFile = 'mocks/file.txt';

  it('should publish a package and return the url  when pass a buffer', async () => {
    const buffer = createReadStream(mockFile);

    const { url } = await publishPackage(buffer, fileName);

    expect(isURL(url)).toBe(true);
  });

  it('should publish a package and return the url when pass a path', async () => {
    const { url } = await publishPackage(mockFile, fileName);

    expect(isURL(url)).toBe(true);
  });

  it('should publish a package and return the url when pass a url', async () => {
    const { url } = await publishPackage('http://www.mocky.io/v2/5afc5475310000cb327c5dee', fileName);

    expect(isURL(url)).toBe(true);
  });

  it('should publish a package and return the password when publish a new package', async () => {
    const date = new Date();
    const {
      url, password, fileAlreadyShared,
    } = await publishPackage(mockFile, date.getTime() + fileName);

    expect(isURL(url)).toBe(true);
    expect(fileAlreadyShared).toBe(false);
    expect(typeof password).toBe('string');
  });

  it('should publish a package and not return the password when publish a old package', async () => {
    const date = new Date();
    await publishPackage(mockFile, date.getTime() + fileName, { deleteWhenFinish: false });
    const {
      url, password, fileAlreadyShared,
    } = await publishPackage(mockFile, date.getTime() + fileName);

    expect(isURL(url)).toBe(true);
    expect(password).toBeUndefined();
    expect(fileAlreadyShared).toBe(true);
  });

  it('should return password 123 when set password as 123', async () => {
    const date = new Date();
    const expected = 123;
    const {
      url, password,
    } = await publishPackage(mockFile, date.getTime() + fileName, { password: expected });

    expect(isURL(url)).toBe(true);
    expect(password).toBe(expected);
  });

  it('should return an error when is not passed the fileName', async () => {
    const { error } = await publishPackage(mockFile, null);

    expect(error).toBeInstanceOf(Error);
  });
});
